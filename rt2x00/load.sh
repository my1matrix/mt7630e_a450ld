[ "$(lsmod | grep "eeprom ")" == "" ] &&
  insmod /lib/modules/$(uname -r)/kernel/drivers/misc/eeprom/eeprom.ko
[ "$(lsmod | grep "eeprom_93cx6 ")" == "" ] &&
  insmod /lib/modules/$(uname -r)/kernel/drivers/misc/eeprom/eeprom_93cx6.ko
[ "$(lsmod | grep "crc_ccitt ")" == "" ] &&
  insmod /lib/modules/$(uname -r)/kernel/lib/crc-ccitt.ko
[ "$(lsmod | grep "cfg80211 ")" == "" ] &&
  insmod /lib/modules/$(uname -r)/kernel/net/wireless/cfg80211.ko
[ "$(lsmod | grep "mac80211 ")" == "" ] &&
  insmod /lib/modules/$(uname -r)/kernel/net/mac80211/mac80211.ko
KLIBPATH="/lib/modules/$(uname -r)/kernel/drivers/net/wireless/mt7630e"
[ "$(lsmod | grep "rt2x00lib ")" == "" ] &&
  insmod $KLIBPATH/rt2x00lib.ko
[ "$(lsmod | grep "rt2x00pci ")" == "" ] &&
  insmod $KLIBPATH/rt2x00pci.ko
[ "$(lsmod | grep "rt2x00mmio ")" == "" ] &&
  insmod $KLIBPATH/rt2x00mmio.ko
[ "$(lsmod | grep "rt2800lib ")" == "" ] &&
  insmod $KLIBPATH/rt2800lib.ko
[ "$(lsmod | grep "rt2800pci ")" == "" ] &&
  insmod $KLIBPATH/rt2800pci.ko
